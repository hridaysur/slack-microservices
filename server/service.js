'use strict';

var service = require('express')();
const ServiceRegistry = require('./serviceRegistry');
const serviceRegistry = new ServiceRegistry();

service.set('serviceRegistry', serviceRegistry);

service.put('/service/:intent/:port',(req, res, next)=>{
    const serviceIntent = req.params.intent;
    const servicePort = req.params.port;

    const ipAddress = req.connection.remoteAddress.includes('::') ? `[${req.connection.remoteAddress}]` : req.connection.remoteAddress;

    serviceRegistry.add(serviceIntent,ipAddress,servicePort);
    res.json({
        result:`${serviceIntent} at ${ipAddress}:${servicePort}`
    });
});

module.exports = service;