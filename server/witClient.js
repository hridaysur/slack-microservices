'use strict';


const request = require('superagent');

function handelWitResponse(res){
    return res.entities;
}


module.exports = function witClient(token){
    const ask = function ask(message,cb){

        request.get('https://api.wit.ai/message')
        .set('Authorization', 'Bearer '+token)
        .query({v:'20180801'})
        .query({q: message})
        .end((err, res) => {
            if(err){
                return cb(err);
            }
            if(res.statusCode != 200) {
                return cb('expected status code was 200 but the actuall is ' + res.statusCode);
            }

            var witResponse = handelWitResponse(res.body);

            return cb(null, witResponse);
        })


        console.log('ask:', message);
        console.log('token:', token);
    };

    return {
        ask:ask
    };

};