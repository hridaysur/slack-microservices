'use strict';

const  RTMClient  = require('@slack/client').RTMClient;
let rtm = null;
var nlp = null;
let registry = null;
 
function handleOnAuthenticated(rtmStartData){
    console.log(`logged in as ${rtmStartData.self.name} of team ${rtmStartData.team.name}, but not yet connected. `)
}

function addAuthenticated(rtm,handler){
    rtm.on('authenticated', handler);
}

function handleOnMessage(message){

    if(message.text.toLowerCase().includes('anjali')){
        nlp.ask(message.text, (err, res) => {
            if(err) {
                console.log(err);
                return;
            }

            try{
                if(!res.intent || !res.intent[0] || !res.intent[0].value){
                    throw new Error('could not extract intent');
                }

                const intent = require('./intents/'+res.intent[0].value+'intent');

                intent.process(res,registry, function(err, response){
                    if(err){
                        console.log(err);
                        return;
                    }

                    return rtm.sendMessage(response, message.channel);
                })


            } catch(err){
                console.log(err);
                console.log(res);
                rtm.sendMessage('Sorry, i don\'t know what you are talking about', message.channel);
            }
        });
    }  
}


module.exports.init = function slackClient(token,logLevel,nlpClient, registryService){
// The client is initialized and then started to get an active connection to the platform
        rtm = new RTMClient(token,{LogLevel:logLevel});
        nlp = nlpClient;
        registry = registryService;
        addAuthenticated(rtm,handleOnAuthenticated);
        rtm.on('message', handleOnMessage);
        return rtm;
};

module.exports.addAuthenticated = addAuthenticated;
