'use strict';

const request = require('superagent');


module.exports.process = function process(intentData,registry, cb){
    if(intentData.intent[0].value !== 'time'){
        return cb(new Error(`expected intent timeintent but got ${intentData.intent[0].value} `))
    }

    if(!intentData.location) {
        return cb(new Error('missing location in time intent'));
    }

    else{
        const location = intentData.location[0].value.replace(/,.?anjali/i,'');
        const service = registry.get('time');

        if(!service) return cb(false,'No Service found');
        
        request.get(`http://${service.ip}:${service.port}/service/`+ location, (err, res) => {
            if(err || res.statusCode !== 200 || !res.body.result){
                console.log(err);

                return cb(false , `I had problem finding our time in ${location}`)
            } else {
                return cb(false, `In ${location} the time is ${res.body.result}`);
            }
        });
    }
};