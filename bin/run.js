'use strict';

var service = require('../server/service');
var http = require('http');
var server = http.createServer(service);
var slackClient = require('../server/slackClient.js');

//Wit Token
const witToken = process.env.wit_token;
const witClient = require('../server/witClient.js')(witToken);

// An access token (from your Slack app or custom integration - usually xoxb)
const slackToken = process.env.slack_api;
const slackLogLevel ='verbose';


const serviceRegistry = service.get('serviceRegistry');

const rtm = slackClient.init(slackToken, slackLogLevel,witClient,serviceRegistry);
rtm.start();

slackClient.addAuthenticated(rtm,() => {
    server.listen(3000,()=>{
        console.log(`node server started on ${server.address().port} in ${service.get('env')} mode.`);
    })
})


