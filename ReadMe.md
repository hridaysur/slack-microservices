Slack Microservices
 This a Slack Microservices developed using NodeJs, express.
 
 Development server
 Run npm run start to run the main chat app. Run npm run start to run all the microservices individually.
 
 Code scaffolding
 All the dependencies can be installed via npm install.
 
 Functionalities
 1) Created Slack bot using slack API on the main application.
 2) Interact with Wit.ai to get a response.
 3) Call geolocation API to get the latitude & longitude of the location.
 4) Call timezone API to get the localtime of the given location.
 5) All the services have been registered using class called registerServices.
 6) Implement methode for all the Microservices to announce there Port, IP every 15 second to main app